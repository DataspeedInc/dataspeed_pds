This is a temporary hack to trick the build script into uploading the lcm debian packages when uploading built debian packages

Debian packages for Ubuntu downloaded from:  
https://launchpad.net/ubuntu/+source/lcm

Official Ubuntu packages available for Bionic and newer:  
https://packages.ubuntu.com/search?keywords=liblcm&searchon=names
