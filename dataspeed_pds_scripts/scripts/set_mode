#!/usr/bin/env python

# Software License Agreement (BSD License)
#
# Copyright (c) 2017-2020, Dataspeed Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
# 
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright notice,
#       this list of conditions and the following disclaimer in the documentation
#       and/or other materials provided with the distribution.
#     * Neither the name of Dataspeed Inc. nor the names of its
#       contributors may be used to endorse or promote products derived from this
#       software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import rospy
import sys
from dataspeed_pds_msgs.msg import Mode

def main():
  msg = Mode()
  try:
    try:
      if int(sys.argv[1]) > 2 or int(sys.argv[1]) < 0:
        raise ValueError("Mode out of bounds")
      else:
        msg.mode = int(sys.argv[1])
    except ValueError:
      if sys.argv[1] == "auto":
        msg.mode = 0
      elif sys.argv[1] == "manual":
        msg.mode = 1
      elif sys.argv[1] == "valet":
        msg.mode = 2
      else:
        print("Error: Invalid value for mode %s" % sys.argv[1])
        print("Try one of these:")
        print("    0, auto, 1, manual, 2, valet")
        return
  except IndexError:
    print("Error: Invalid or missing arguments.")
    print("Format:")
    print("    set_mode <auto/manual/valet>")
    return
  rospy.init_node('mode', anonymous=True);
  pub = rospy.Publisher('/pds/mode', Mode, queue_size=1)
  rospy.sleep(0.5)
  pub.publish(msg)
  rospy.sleep(0.5)

if __name__ == '__main__':
  main()
