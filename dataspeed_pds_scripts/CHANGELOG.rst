^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package dataspeed_pds_scripts
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.6 (2020-11-13)
------------------
* Updated license year for 2020
* Renamed PDS to iPDS
* Contributors: Michael Lohrer

1.0.5 (2020-07-28)
------------------

1.0.4 (2020-07-24)
------------------

1.0.3 (2020-07-09)
------------------
* Increase CMake minimum version to 3.0.2 to avoid warning about CMP0048
  http://wiki.ros.org/noetic/Migration#Increase_required_CMake_version_to_avoid_aut
* Contributors: Kevin Hallenbeck

1.0.2 (2018-06-29)
------------------

1.0.1 (2018-06-28)
------------------

1.0.0 (2018-04-25)
------------------
* Updated scripts for new topics and message types
* Contributors: Kevin Hallenbeck

0.1.2 (2018-04-11)
------------------

0.1.1 (2017-09-07)
------------------
* Fixed script licenses
* Contributors: Kevin Hallenbeck

0.1.0 (2017-09-07)
------------------
* Initial release
* Contributors: Kevin Hallenbeck
