^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package dataspeed_pds_msgs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.6 (2020-11-13)
------------------
* Renamed PDS to iPDS
* Support all 3 internal temp sensors and up to 4 external
* Contributors: Michael Lohrer

1.0.5 (2020-07-28)
------------------

1.0.4 (2020-07-24)
------------------

1.0.3 (2020-07-09)
------------------
* Increase CMake minimum version to 3.0.2 to avoid warning about CMP0048
  http://wiki.ros.org/noetic/Migration#Increase_required_CMake_version_to_avoid_aut
* Contributors: Kevin Hallenbeck

1.0.2 (2018-06-29)
------------------

1.0.1 (2018-06-28)
------------------

1.0.0 (2018-04-25)
------------------
* Dynamic channel size (12,24,36,48) in status message
* Combine status and current for one large status message
* Contributors: Kevin Hallenbeck

0.1.2 (2018-04-11)
------------------
* Added bag migration rules for migrating old recorded messages
* Fixed inverter status
* Contributors: Eric Myllyoja, Kevin Hallenbeck

0.1.1 (2017-09-07)
------------------

0.1.0 (2017-09-07)
------------------
* Initial release
* Contributors: Eric Myllyoja, Kevin Hallenbeck
