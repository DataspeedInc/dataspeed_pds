^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package dataspeed_pds_rqt
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.6 (2020-11-13)
------------------
* Updated license year for 2020
* Renamed PDS to iPDS
* Contributors: Michael Lohrer

1.0.5 (2020-07-28)
------------------

1.0.4 (2020-07-24)
------------------

1.0.3 (2020-07-09)
------------------
* Use setuptools instead of distutils for python
  http://wiki.ros.org/noetic/Migration#Setuptools_instead_of_Distutils
* Increase CMake minimum version to 3.0.2 to avoid warning about CMP0048
  http://wiki.ros.org/noetic/Migration#Increase_required_CMake_version_to_avoid_aut
* Contributors: Kevin Hallenbeck

1.0.2 (2018-06-29)
------------------
* Removed unnecessary dependency
* Contributors: Kevin Hallenbeck

1.0.1 (2018-06-28)
------------------
* Initial release
* Contributors: Eric Myllyoja, Kevin Hallenbeck
